// This is the code I give you to “jump to the webpage”   这是我给大家的跳转网页代码
// Code By Xiaozhu
//请带参数url or go 例如：http://xiaozhu2007.gitee.io/jump.html?go=https://www.baidu.com
     
function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}
var myurl=GetQueryString("url");
if(myurl !=null && myurl.toString().length>1)
{
window.location.href=GetQueryString("url");
}
else
{
var mygo=GetQueryString("go");
if(mygo !=null && mygo.toString().length>1)
{
window.location.href=GetQueryString("go");
}
else
{
window.location.href="https://xiaozhu2007.gitee.io";//If there is no parameter, the site to jump to！  如果没有参数，则 跳转的网站
}
}

